import React from 'react';
import { Text, View } from 'react-native';

const HelloWorldApp = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}>
      <Text>INI ADALAH TEKS, JADI INI ADALAH CONTOH DARI PENGGUNAAN TEKS</Text>
    </View>
  )
}
export default HelloWorldApp;